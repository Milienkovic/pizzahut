<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = [
        'total'
    ];

    protected $hidden = [
        'customer_id'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function pizzas()
    {
        return $this->belongsToMany('App\Pizza')->withTimestamps();
    }
}
