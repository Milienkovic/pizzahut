<?php

namespace App\Observers;

use App\Order;
use App\Pizza;

class PizzaObserver
{
    public function deleting(Pizza $pizza)
    {
        $pizza->orders()->detach(Order::all()->pluck('id'));
    }
}
