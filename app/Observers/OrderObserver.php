<?php

namespace App\Observers;

use App\Order;
use App\Pizza;

class OrderObserver
{
    public function deleting(Order $order)
    {
        $order->pizzas()->detach(Pizza::all()->pluck('id'));
    }
}
