<?php

namespace App\Observers;

use App\Customer;

class CustomerObserver
{
    public function deleting(Customer $customer)
    {
        $customer->orders()->delete();
    }
}
