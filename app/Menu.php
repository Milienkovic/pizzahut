<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //

    protected $fillable = [
        'name',
        'delivery_cost'
    ];

    public function pizzas()
    {
        return $this->hasMany('App\Pizza');
    }
}
