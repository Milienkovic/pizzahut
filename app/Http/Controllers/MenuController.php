<?php

namespace App\Http\Controllers;

use App\Http\Resources\MenuResource;
use App\Menu;
use Illuminate\Http\Response;

class MenuController extends Controller
{


    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        $menu = Menu::with('pizzas')->first();
        if ($menu)
            return \response(new MenuResource($menu), 200);

        return \response('Entity not found!', 404);
    }

}
