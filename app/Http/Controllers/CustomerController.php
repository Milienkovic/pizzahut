<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Resources\CustomerResource;
use App\Order;
use App\Pizza;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use function Symfony\Component\String\u;

class CustomerController extends Controller
{

    private $customerValidationRules = [
        'first_name' => 'required |regex:"^[A-Z][a-z]"',
        'last_name' => 'required |regex:"^[A-Z][a-z]"',
        'phone' => 'required|regex:"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$"',
        'address' => 'required |regex:"^[A-Z][a-z]"'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $customers = Customer::all();
        return response(CustomerResource::collection($customers), 200);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), $this->customerValidationRules);

        if ($validation->fails()) {
            $errors = array('errors' => $validation->errors(), 'success' => false);
            return \response($errors, 400);
        } else {
            $u = User::find($request->input('user_id'));
            error_log($u->customer);
            if ($u->customer) {
                $customer = $u->customer;
                $this->handleOrder($request, $customer);
                return \response(new CustomerResource($customer), 201);
            } else {
                $c = $this->createCustomer($request);
                $c->user()->associate($u);
                $c->save();
                $this->handleOrder($request, $c);
                return \response(new CustomerResource($c), 201);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function show($user)
    {
        $customer = Customer::where('user_id', $user)->first();
        if ($customer)
            return \response(new CustomerResource($customer), 200)
                ->header('Content-Type', 'application/json');
        return \response('Entity not found!', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), $this->customerValidationRules);

        if ($validation->fails()) {
            $errors = array('errors' => $validation->errors(), 'success' => false);
            return \response($errors, 400);
        } else {
            $customer = Customer::find($id);
            if ($customer)
                return \response(new CustomerResource($this->saveCustomer($request, $customer)), 200);
            return \response('Unable to update Entity', 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        if ($customer->delete()) {
            return \response($id, 200);
        }

        return \response('Unable to delete entity!', 400);
    }

    private function saveCustomer(Request $request, Customer $customer)
    {
        $customer->first_name = $request->input('first_name');
        $customer->last_name = $request->input('last_name');
        $customer->phone = $request->input('phone');
        $customer->address = $request->input('address');
        $customer->save();
        return $customer;
    }

    private function createCustomer(Request $request)
    {
        $customer = new Customer();
        $customer->first_name = $request->input('first_name');
        $customer->last_name = $request->input('last_name');
        $customer->phone = $request->input('phone');
        $customer->address = $request->input('address');
        return $customer;
    }

    private function handleOrder(Request $request, Customer $relatedCustomer)
    {
        $order = new Order();
        $order->total = $request->input('total');
        $order->customer()->associate($relatedCustomer);
        $order->save();
        $pizzas = Pizza::find($request->input('pizzas'));
        $order->pizzas()->attach($pizzas);
    }
}
