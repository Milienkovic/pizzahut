<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use function response;

class UserController extends Controller
{
    private $successStatus = 200;
    private $userValidationRules = [
        'name' => 'required |regex:"^[A-Z][a-z]"',
        'email' => 'required|regex:/^\S+@\S+\.\S+$/',
        'password' => 'required|regex:"^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$"',
        'confirm_password' => 'required|same:password'
    ];

    /**
     * Login.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->input('email'),
            'password' => $request->input('password')])) {
            $user = Auth::user();
            $authToken = $user->createToken('PizzaHut')->accessToken;
            return response()->json(['success' => 'User has been logged in'], $this->successStatus)
                ->header('Authorization', 'Bearer ' . $authToken);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    /**
     * Register new user.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function registration(Request $request)
    {
        $validation = Validator::make($request->all(), $this->userValidationRules);

        if ($validation->fails()) {
            $errors = array('errors' => $validation->errors(), 'success' => false);
            return response()->json([$errors], 400);
        } else {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $authToken = $user->createToken('PizzaHut')->accessToken;
            return response()->json(['success' => 'User has been registered successfully'], $this->successStatus)
                ->header('Authorization', 'Bearer ' . $authToken);
        }
    }

    /**
     * Authenticated user's details.
     *
     * @return JsonResponse
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['user' => $user], $this->successStatus);
    }
}

