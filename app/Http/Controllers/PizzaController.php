<?php

namespace App\Http\Controllers;

use App\Http\Resources\PizzaResource;
use App\Order;
use App\Pizza;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PizzaController extends Controller
{
    private $pizzaValidationRules = [
        'name' => 'required |regex:"^[A-Z][a-z]"',
        'description' => 'required',
        'price' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param $menu
     * @return Response
     */
    public function index($menu)
    {
        $pizzas = Pizza::all()->where('menu_id', $menu);
        return \response(PizzaResource::collection($pizzas), 200);
    }

    /**
     * Display a list or order's pizzas.
     *
     * @param $order
     * @return Response
     */
    public function ordersPizzas($order)
    {
        $orderEntity = Order::find($order);
    //todo ref
        if ($orderEntity) {
            $pizzas = $orderEntity->pizzas;
            return \response(PizzaResource::collection($pizzas), 200);
        }

        return \response('No Items found', 404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $menu
     * @return Response
     */
    public function store(Request $request, $menu)
    {
        $validation = Validator::make($request->all(), $this->pizzaValidationRules);

        if ($validation->fails()) {
            $errors = array('errors' => $validation->errors(), 'success' => false);
            return \response($errors, 400);
        } else {
            $pizza = new Pizza();
            $pizza->menu_id = $menu;
            $pizza = $this->savePizza($request, $pizza);
            return \response(new PizzaResource($pizza), 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $pizza = Pizza::find($id);
        if ($pizza)
            return \response(new PizzaResource($pizza), 200)
                ->header('Content-Type', 'application/json');
        return \response('Entity not found!', 404)
            ->header('Content-Type', 'application/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), $this->pizzaValidationRules);

        if ($validation->fails()) {
            $errors = array('errors' => $validation->errors(), 'success' => false);
            return \response($errors, 400)
                ->header('Content-Type', 'application/json');
        } else {

            $pizza = Pizza::find($id);
            if ($pizza)
                return \response(new PizzaResource($this->savePizza($request, $pizza)))
                    ->header('Content-Type', 'application/json');

            return \response('Unable to update entity', 400)
                ->header('Content-Type', 'application/json');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $pizza = Pizza::find($id);
        $pizza->orders()->detach();

        if ($pizza->delete()) {
            return \response($id, 200)
                ->header('Content-Type', 'application/json');
        }

        return \response('Unable to delete entity!', 400)
            ->header('Content-Type', 'application/json');
    }

    private function savePizza(Request $request, Pizza $pizza)
    {
        $pizza->name = $request->input('name');
        $pizza->description = $request->input('description');
        $pizza->price = $request->input('price');
        $pizza->save();
        return $pizza;
    }
}
