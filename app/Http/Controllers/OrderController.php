<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Resources\OrderResource;
use App\Order;
use App\Pizza;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    private $orderValidationRules = [
        'total' => 'required|numeric|min:0'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param $customerIds
     * @return Response
     */
    public function index($customerIds)
    {
        $customerIds= Customer::where('user_id',$customerIds)->pluck('id')->toArray();
        $orders = Order::where('customer_id', $customerIds)->orderBy('created_at', 'desc')->get();
        return response(OrderResource::collection($orders), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $customer
     * @return Response
     */
    public function store(Request $request, $customer)
    {
        $validation = Validator::make($request->all(), $this->orderValidationRules);

        if ($validation->fails()) {
            $errors = array('errors' => $validation->errors(), 'success' => false);
            return \response($errors, 400);
        } else {
            $order = new Order();
            $order->customer_id = $customer;
            $order->total = $request->input('total');
            $order->save();

            $pizzas = Pizza::find($request->input('pizzas'));
            $order->pizzas()->attach($pizzas);
            $order['pizzas'] = $pizzas;
            return response(new OrderResource($order), 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $order = Order::with('pizzas')->find($id);

        if ($order)
            return \response(new OrderResource($order), 200);

        return \response('Entity not found!', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), $this->orderValidationRules);
        if ($validation->fails()) {
            $errors = array('errors' => $validation->errors(), 'success' => false);
            return \response($errors, 400);
        } else {
            $order = Order::find($id);

            if ($order) {
                $order->total = $request->input('total');
                return \response(new OrderResource($order), 200);
            }
            return \response('Unable to update entity!', 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        if ($order->delete()) {
            return \response($id, 200);
        }
        return \response('Unable to delete entity!', 400);
    }
}
