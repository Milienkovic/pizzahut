<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'address'
    ];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
