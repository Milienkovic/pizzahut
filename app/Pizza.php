<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
        'price'
    ];

    protected $hidden = [
        'menu_id', 'pivot'
    ];

    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order')->withTimestamps();
    }
}
