<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pizza;
use Faker\Generator as Faker;

$factory->define(Pizza::class, function (Faker $faker) {

    $pizzaNames = ['Margherita', 'Americana', 'Schiacciata', 'Fattoria', 'Romana', 'Emiliana', 'Montanara', 'Pugliese', 'Napoletana',
        'Crudo', 'Quattro Formaggi', 'Frutti di Mare', 'Carbonara', 'Quattro Stagioni', 'Marinara'];
    $pizzaDesc = ['Tomato sauce, mozzarella, and oregano', 'Tomato sauce, garlic and basil', 'Tomato sauce, mozzarella, mushrooms, ham, artichokes, olives, and oregano',
        'Tomato sauce, mozzarella and Parma ham', 'Tomato sauce, mozzarella, oregano, and onions', 'Tomato sauce, mozzarella, anchovies, capers, and oregano',
        'Tomato sauce, mozzarella, peppers, peas, porchetta(Italian spit - roasted pork)', 'Tomato sauce, mozzarella, ham, and oregano'];

    return [
        'name' => $faker->randomElement($pizzaNames),
        'description' => $faker->randomElement($pizzaDesc),
        'price' => $faker->randomFloat(2, 1, 50),
        'menu_id' => 1
    ];
});
