<?php

/** @var Factory $factory */

use App\Menu;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Menu::class, function (Faker $faker) {
    return [
        'name' => 'Pizza Menu',
        'delivery_cost' => 9.95
    ];
});
