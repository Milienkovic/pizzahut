

## About Project
After starting Apache and mySQL servers from XAMP, create mySQL database named pizzahut from phpMyAdmin

###Rename .env.example to .env file in the root directory, change db data there if needed, username, password...
If php artisan is not recognised as a command please run 
#### composer update

After composer installs all required dependencies, run

#### php artisan migrate:fresh

It will populate db with required tables, then run

#### php artisan db:seed

to generate app key run
###php artisan key:generate

to insert some dummy data into database

### php artisan passport:install
to generate some auth stuff

At the end run 
###php artisan serve 
to start backend server

server starts at http://127.0.0.1:8000
