<?php

use App\Pizza;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use const App\Http\Controllers\PizzaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//pizzas
//Route::apiResource('pizzas','PizzaController');
Route::get('/{menu}/pizzas', 'PizzaController@index');
Route::get('/pizzas/{id}', 'PizzaController@show');
Route::get('/orders/{order}/pizzas', 'PizzaController@ordersPizzas');
Route::post('/{menu}/pizzas', 'PizzaController@store');
Route::put('/pizzas/{id}', 'PizzaController@update');
Route::delete('/pizzas/{id}', 'PizzaController@destroy');

//menus
Route::get('/menus', 'MenuController@show');

//customers
Route::get('/customers', 'CustomerController@index');
Route::get('/customers/{user}', 'CustomerController@show');
Route::post('/customers', 'CustomerController@store');
Route::put('/customers/{id}', 'CustomerController@update');
Route::delete('/customers/{id}', 'CustomerController@destroy');

//orders
Route::get('/orders/{id}', 'OrderController@show');
Route::post('/{customer}/orders', 'OrderController@store');
Route::put('/orders/{id}', 'OrderController@update');
Route::delete('/orders/{id}', 'OrderController@destroy');


//auth
Route::post('/login', 'UserController@login');
Route::post('/registration', 'UserController@registration');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/details', 'UserController@details');
    Route::get('/{customer}/orders', 'OrderController@index');
});

